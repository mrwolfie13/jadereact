import React from 'react'
import './card.css'
import {Link} from 'react-router-dom'
const read = "Read More >"

function HomeCard(props) {
  return (
            <div className="hcard">
                <div  >
                    <img src={props.img} alt="" className="hcimg"/>
                </div>
                <div className="hcbody">
                    <h2 className="hctitle">{props.title}</h2>
                    <ul>
                        <Link to={props.red}>
                        <button className='hcbtn'>{read} </button>
                        </Link>
                    </ul>
    
                    
                </div>
            </div>
        )
    }
    


export default HomeCard