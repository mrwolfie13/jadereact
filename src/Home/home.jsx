import React from 'react'
import './Home.css'
import Typewriter from "typewriter-effect";
import HomeCard from '../Cards/homeCard'
import cardi from "../assets/PP_Paper.png"
import {Link} from "react-router-dom"
import card2 from "../assets/ECO-LITE-Folding-A-Frame-Sign-Board-A1-Track-Trace-Social-Distance-Signs.jpg-11.png"
import Slide from './Slide';
import card3 from "../assets/flag.png"
import mini from "../assets/minirollup.png"
import Wa from './wa';

function Home() {
  return (
    <div className='home'>
      <div className='headin'>
        <h2>JADESIGN ADVERTISING</h2>
        <p>
          <Typewriter
            onInit={(typewriter)=> {
            typewriter
            .typeString("One Stop shop for Advertising Sign Supplies and Display Products ")
            .pauseFor(1000)
            .deleteAll()
            .typeString("Display Products")
            .pauseFor(1000)
            .deleteAll()
            .typeString("Digital Printing Media")
            .pauseFor(1000)
            .typeString("Flags")
            .pauseFor(1000)
            .typeString("Backdrops")
            .pauseFor(1000)
            .deleteAll()
            .typeString("One Stop shop for Advertising Sign Supplies and Display Products ")
            .start();
            }}
          />
        </p>

      </div>
      <div className='slides'>
            <Slide/>
        </div>
      <div className='link'>
        <Link to='/'><h2>PRODUCTS</h2></Link>
        
        
        <div className='wrapper'>
          <HomeCard img = {cardi} title="Advertising Accessories" red="/Advd" />
          <HomeCard img = {card2} title="Display Products" red="/Product" />
          <HomeCard img = {card3} title="Flags" red="/Flags" />
          <HomeCard img = {mini} title="Backdrops" red="/Backdrop" />
        </div>
      </div>

      <Wa/>
      {/* <div className='more'>
        <Link to="/Product">{more}</Link>
      </div> */}
      
       

    </div>
  )
}

export default Home